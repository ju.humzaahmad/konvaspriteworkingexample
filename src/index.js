import React from "react";
import ReactDOM from "react-dom";
import { Stage, Layer, Sprite } from "react-konva";
import useImage from "use-image";
import myBlob from "./BrownDogSpriteSheet100x100Each.png";

import "./styles.css";

const animations = {
  backHurt: [0,0,100,100,100,0,100,100,200,0,100,100,300,0,100,100,400,0,100,100,500,0,100,100,600,0,100,100,700,0,100,100,800,0,100,100,900,0,100,100,1000,0,100,100,1100,0,100,100],
  backIdle: [0,100,100,100,100,100,100,100,200,100,100,100,300,100,100,100,400,100,100,100,500,100,100,100,600,100,100,100,700,100,100,100,800,100,100,100,900,100,100,100,1000,100,100,100,1100,100,100,100],
  backWalking: [0,200,100,100,100,200,100,100,200,200,100,100,300,200,100,100,400,200,100,100,500,200,100,100,600,200,100,100,700,200,100,100,800,200,100,100,900,200,100,100,1000,200,100,100,1100,200,100,100],
  dying: [0,300,100,100,100,300,100,100,200,300,100,100,300,300,100,100,400,300,100,100,500,300,100,100,600,300,100,100,700,300,100,100,800,300,100,100,900,300,100,100,1000,300,100,100,1100,300,100,100],
  frontHurt: [0,400,100,100,100,400,100,100,200,400,100,100,300,400,100,100,400,400,100,100,500,400,100,100,600,400,100,100,700,400,100,100,800,400,100,100,900,400,100,100,1000,400,100,100,1100,400,100,100],
  frontIdleBlinking: [0,500,100,100,100,500,100,100,200,500,100,100,300,500,100,100,400,500,100,100,500,500,100,100,600,500,100,100,700,500,100,100,800,500,100,100,900,500,100,100,1000,500,100,100,1100,500,100,100],
  frontIdle: [0,600,100,100,100,600,100,100,200,600,100,100,300,600,100,100,400,600,100,100,500,600,100,100,600,600,100,100,700,600,100,100,800,600,100,100,900,600,100,100,1000,600,100,100,1100,600,100,100],
  frontWalking: [0,700,100,100,100,700,100,100,200,700,100,100,300,700,100,100,400,700,100,100,500,700,100,100,600,700,100,100,700,700,100,100,800,700,100,100,900,700,100,100,1000,700,100,100,1100,700,100,100],
  leftHurt: [0,800,100,100,100,800,100,100,200,800,100,100,300,800,100,100,400,800,100,100,500,800,100,100,600,800,100,100,700,800,100,100,800,800,100,100,900,800,100,100,1000,800,100,100,1100,800,100,100],
  leftIdleBlinking: [0,900,100,100,100,900,100,100,200,900,100,100,300,900,100,100,400,900,100,100,500,900,100,100,600,900,100,100,700,900,100,100,800,900,100,100,900,900,100,100,1000,900,100,100,1100,900,100,100],
  leftIdle: [0,1000,100,100,100,1000,100,100,200,1000,100,100,300,1000,100,100,400,1000,100,100,500,1000,100,100,600,1000,100,100,700,1000,100,100,800,1000,100,100,900,1000,100,100,1000,1000,100,100,1100,1000,100,100],
  leftWalking: [0,1100,100,100,100,1100,100,100,200,1100,100,100,300,1100,100,100,400,1100,100,100,500,1100,100,100,600,1100,100,100,700,1100,100,100,800,1100,100,100,900,1100,100,100,1000,1100,100,100,1100,1100,100,100],
  rightHurt: [0,1200,100,100,100,1200,100,100,200,1200,100,100,300,1200,100,100,400,1200,100,100,500,1200,100,100,600,1200,100,100,700,1200,100,100,800,1200,100,100,900,1200,100,100,1000,1200,100,100,1100,1200,100,100],
  rightIdleBlinking: [0,1300,100,100,100,1300,100,100,200,1300,100,100,300,1300,100,100,400,1300,100,100,500,1300,100,100,600,1300,100,100,700,1300,100,100,800,1300,100,100,900,1300,100,100,1000,1300,100,100,1100,1300,100,100],
  rightIdle: [0,1400,100,100,100,1400,100,100,200,1400,100,100,300,1400,100,100,400,1400,100,100,500,1400,100,100,600,1400,100,100,700,1400,100,100,800,1400,100,100,900,1400,100,100,1000,1400,100,100,1100,1400,100,100],
  rightWalking: [0,1500,100,100,100,1500,100,100,200,1500,100,100,300,1500,100,100,400,1500,100,100,500,1500,100,100,600,1500,100,100,700,1500,100,100,800,1500,100,100,900,1500,100,100,1000,1500,100,100,1100,1500,100,100]
};

function App() {
  const spriteRef = React.useRef(null);
  const stageRef = React.useRef(null);
  const [fps] = React.useState(30);

  const refreshCanvas = () => {spriteRef.current.start();};

  React.useEffect(() => {refreshCanvas();}, []);

  React.useEffect(() => {spriteRef.current.start();}, [fps]);
  
  const DogbackHurt = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={50} animation="backHurt" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogbackIdle = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={150} animation="backIdle" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogbackWalking = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={250} animation="backWalking" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const Dogdying = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={350} animation="dying" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogfrontHurt = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={450} animation="frontHurt" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogfrontIdleBlinking = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={550} animation="frontIdleBlinking" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogfrontIdle = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={650} animation="frontIdle" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogfrontWalking = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={750} animation="frontWalking" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogleftHurt = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={850} animation="leftHurt" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogleftIdleBlinking = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={950} animation="leftIdleBlinking" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogleftIdle = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={1050} animation="leftIdle" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogleftWalking = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={1150} animation="leftWalking" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogrightHurt = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={1250} animation="rightHurt" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogrightIdleBlinking = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={1350} animation="rightIdleBlinking" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogrightIdle = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={1450} animation="rightIdle" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  const DogrightWalking = () => {
    const [image] = useImage(myBlob);
    return (<Sprite key={"sprite-" + fps} ref={spriteRef} image={image} x={50} y={1550} animation="rightWalking" animations={animations} frameRate={fps} frameIndex={0} />);
  };
  return (
    <div className="App">
      <h1>Hello CodeSandbox</h1>
      <h2>Start editing to see some magic happen!</h2>
      <Stage width={5000} height={5000}>
        <Layer ref={stageRef}>
          <DogrightIdle />
          <DogbackHurt />
          <DogbackIdle />
          <DogbackWalking />
          <Dogdying />
          <DogfrontHurt />
          <DogfrontIdleBlinking />
          <DogfrontIdle />
          <DogfrontWalking />
          <DogleftHurt />
          <DogleftIdleBlinking />
          <DogleftIdle />
          <DogleftWalking />
          <DogrightHurt />
          <DogrightIdleBlinking />
          <DogrightIdle />
          <DogrightWalking />
        </Layer>
      </Stage>
    </div>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
